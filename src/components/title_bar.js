import React from "react";
import '../components/title_bar.css';

const TitleBar = () => (<div
    className="menu_box">
    <ul className="menu_style">
        <li className="menu_title">MU Software Solutions</li>
        <li to="">HOME</li>
        <li>ABOUT US</li>
        <li>SERVICES</li>
        <li>OUR WORK</li>
        <li>CONTACT</li>
    </ul>
</div>
)

export default TitleBar;